<?php

function enqueue_parent_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}

add_action('wp_enqueue_scripts', 'enqueue_parent_styles');

add_filter('tiny_mce_before_init', 'prefix_filter_tiny_mce_before_init');
function prefix_filter_tiny_mce_before_init( $options ) {

    if ( ! isset( $options['extended_valid_elements'] ) ) {
        $options['extended_valid_elements'] = 'style';
    } else {
        $options['extended_valid_elements'] .= ',style';
    }

    if ( ! isset( $options['valid_children'] ) ) {
        $options['valid_children'] = '+body[style]';
    } else {
        $options['valid_children'] .= ',+body[style]';
    }

    if ( ! isset( $options['custom_elements'] ) ) {
        $options['custom_elements'] = 'style';
    } else {
        $options['custom_elements'] .= ',style';
    }

    return $options;
}